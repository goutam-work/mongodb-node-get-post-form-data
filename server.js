const express = require("express");
const bodyParser = require("body-parser");
const MongoClient = require('mongodb').MongoClient;
const app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.set('view engine','ejs');
var db;



MongoClient.connect('mongodb://goutam:goutam!23@ds139438.mlab.com:39438/star-wars-quotes',(err,database) => {
	if (err) return console.log(err);
	db= database;
	app.listen(3001,()=>{
		console.log('listening on 3001');
	});
});



app.get("/",(req,res) => {
	/*db.collection('quotes').find().toArray((err,result) => {
		if(err) return console.log(err);
		res.render('index.ejs',{quotes:result});
	});*/

	db.collection('users').find().toArray((err,result) => {
		if(err) return console.log(err);
		res.render('index.ejs',{users:result});
	});

});



app.post('/users', (req, res) => {
  db.collection('users').save(req.body, (err, result) => {
    if (err) return console.log(err);

    console.log('saved to database');
    res.redirect('/');
  });
})